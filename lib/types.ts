import * as pulumi from "@pulumi/pulumi";

export type TransformationFunc = (o: any, opts: pulumi.CustomResourceOptions) => void;
