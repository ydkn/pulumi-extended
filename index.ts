import { ComponentResource } from "./lib/componentResource";
import { TransformationFunc } from "./lib/types";

export { ComponentResource, TransformationFunc };
